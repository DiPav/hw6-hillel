package homework6;

import org.apache.commons.lang3.ArrayUtils;

import java.util.*;

public class MyArrayList<T> implements MyList<T> {
    private static final int INITIAL_SIZE = 8;
    private static final int SCALE_FACTOR = 2;
    private Object[] arr = new Object[INITIAL_SIZE];
    private int size = 0;

    @Override
    public void add(Object elem) {
        if (arr.length == size) expand();
        arr[size] = elem;
        size++;
    }

    @Override
    public void insert(Object elem, int index) {
        arr = ArrayUtils.insert(index - 1, arr, elem);
    }


    @Override
    public void remove(int index) {
        arr = ArrayUtils.remove(arr, index);
    }

    @Override
    public void remove(Object value) {
        arr = ArrayUtils.removeElement(arr, value);
    }

    @Override
    public int indexOf(Object value) {
        return ArrayUtils.indexOf(arr, value);
    }

    @Override
    public boolean contains(Object value) {
        return ArrayUtils.contains(arr, value);
    }

    @Override
    public boolean containsAll(MyList values) {
        List<T> temp = new ArrayList<>();
        for (int i = 0; i < values.size(); i++) {
            temp.add((T) values.get(i));
        }
        for (Object element : temp) {
            if (!contains(element)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public T get(int index) {
        validateIndex(index);
        return (T) arr[index];
    }

    @Override
    public void set(int index, Object value) {
        validateIndex(index);
        arr[index] = value;
    }

    @Override
    public int size() {
        return size;
    }

    private void expand() {
        int newLength = arr.length * SCALE_FACTOR;
        arr = Arrays.copyOf(arr, newLength);
    }

    private void validateIndex(int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException(index);
        }
    }
}
