package homework6;

import junit.framework.TestCase;

public class MyArrayListTest extends TestCase {
    public void testAdd() {
        MyArrayList<String> arr = new MyArrayList<>();
        arr.add("Hello");
        assertEquals("Hello",arr.get(0));
    }

    public void testInsert() {
        MyArrayList<String> arr = new MyArrayList<>();
        arr.add("Hello");
        arr.add("World");
        arr.insert("Java",2);
        assertEquals("Java",arr.get(1));
    }

    public void testRemove() {
        MyArrayList<String> arr = new MyArrayList<>();
        arr.add("Hello");
        arr.add("World");
        arr.remove(0);
        assertEquals("World",arr.get(0));
    }

    public void testRemoveByValue() {
        MyArrayList<String> arr = new MyArrayList<>();
        arr.add("Hello");
        arr.add("World");
        arr.remove("World");
        assertEquals("Hello",arr.get(0));
    }

    public void testIndexOf() {
        MyArrayList<String> arr = new MyArrayList<>();
        arr.add("Hello");
        arr.add("World");
        assertEquals(1,arr.indexOf("World"));
    }

    public void testContains() {
        MyArrayList<String> arr = new MyArrayList<>();
        arr.add("Hello");
        arr.add("World");
        assertTrue(arr.contains("Hello"));
    }

    public void testContainsAll() {
        MyArrayList<String> arr = new MyArrayList<>();
        MyArrayList<String> arr1 = new MyArrayList<>();
        arr.add("Hello");
        arr.add("World");
        arr.add("!");
        arr1.add("Hello");
        arr1.add("World");
        assertTrue(arr.containsAll(arr1));
    }
    public void testContainsAllFail() {
        MyArrayList<String> arr = new MyArrayList<>();
        MyArrayList<String> arr1 = new MyArrayList<>();
        arr.add("Hello");
        arr.add("World");
        arr.add("!");
        arr1.add("Hello");
        arr1.add("World232");
        assertFalse(arr.containsAll(arr1));
    }

    public void testSet() {
        MyArrayList<String> arr = new MyArrayList<>();
        arr.add("Hello");
        arr.add("World");
        arr.set(1,"Hooo");
        assertEquals("Hooo",arr.get(1));
    }

    public void testSize() {
        MyArrayList<String> arr = new MyArrayList<>();
        arr.add("Hello");
        arr.add("World");
        assertEquals(2,arr.size());
    }
}